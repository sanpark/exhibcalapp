package com.san.exhibitcalapp

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.san.exhibitcalapp.databinding.FragmentFirstBinding
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    var data: String? = null
    var key: String? = null



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
        binding.buttonTestrequest.setOnClickListener{
            val textView = binding.titleTextView//findViewById<TextView>(R.id.text)
            val user = Kcisa("aa", "bb")
            binding.setUser(user)
            /*
             val textView = binding.titleTextView//findViewById<TextView>(R.id.text)
            //binding = DataBindingUtil.setContentView(this.activity, R.layout.fragment_first)
            val user = Kcisa("aa", "bb")
            binding.setUser(user)

            //MainActivityBinding binding = MainActivityBinding.inflate(getLayoutInflater());
            //ListItemBinding binding = ListItemBinding.inflate(layoutInflater, viewGroup, false); // or ListItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item, viewGroup, false);


                        public class BindingViewHolder extends RecyclerView.ViewHolder {
                public RecyclerItemLayoutBinding binding;

                public BindingViewHolder(@NonNull View itemView) {
                    super(itemView);
                    binding = DataBindingUtil.bind(itemView);
                }
            }
             */



            /*
            // ...

            // Instantiate the RequestQueue.
            val queue = Volley.newRequestQueue(this.context)
            val url = "http://api.kcisa.kr/openapi/service/rest/meta16/getkopis02?serviceKey=509c07f2-5641-4289-986f-866deeb75fbb"

            // Request a string response from the provided URL.
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    // Display the first 500 characters of the response string.
                    textView.text = "Response is: ${response.substring(0, 500)}"
                },
                Response.ErrorListener { textView.text = "That didn't work!" })

            // Add the request to the RequestQueue.
            queue.add(stringRequest)
            */
            key = "http://api.kcisa.kr/openapi/service/rest/meta16/getkopis02?serviceKey=509c07f2-5641-4289-986f-866deeb75fbb"

            //val dust = OpenAPI(url)
            //dust.execute()
            Thread {
                data = getXmlDataKcisa()
                System.out.println(data)
                //UiThreadStatement.runOnUiThread(Runnable { textView.setText(data) })
            }.start()

        }
    }

    fun getXmlDataKcisa(): String? {
        val buffer = StringBuffer()
        //val str: String = edit.getText().toString() //EditText에 작성된 Text얻어오기
        //val location: String = URLEncoder.encode(str)
        //val query = "%EC%A0%84%EB%A0%A5%EB%A1%9C"
        val queryUrl = key
        try {
            val url = URL(queryUrl) //문자열로 된 요청 url을 URL 객체로 생성.
            val `is`: InputStream = url.openStream() //url위치로 입력스트림 연결
            val factory = XmlPullParserFactory.newInstance() //xml파싱을 위한
            val xpp = factory.newPullParser()
            xpp.setInput(InputStreamReader(`is`, "UTF-8")) //inputstream 으로부터 xml 입력받기
            var tag: String
            xpp.next()
            var eventType = xpp.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                when (eventType) {
                    XmlPullParser.START_DOCUMENT -> buffer.append("파싱 시작...\n\n")
                    XmlPullParser.START_TAG -> {
                        tag = xpp.name //테그 이름 얻어오기
                        if (tag == "item") {
                            buffer.append("공연 정보 : ")
                            xpp.next()
                            buffer.append(xpp.text) //title 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "title") {
                            buffer.append("공연명 : ")
                            xpp.next()
                            buffer.append(xpp.text) //category 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "creator") {
                            buffer.append("홈페이지 :")
                            xpp.next()
                            buffer.append(xpp.text) //description 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "collectionDb") {
                            buffer.append("공연 상세 DB :")
                            xpp.next()
                            buffer.append(xpp.text) //telephone 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "subjectCategory") {
                            buffer.append("공연 카테고리 :")
                            xpp.next()
                            buffer.append(xpp.text) //address 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "extent") {
                            buffer.append("공연 시간 :")
                            xpp.next()
                            buffer.append(xpp.text) //mapx 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("  ,  ") //줄바꿈 문자 추가
                        } else if (tag == "description") {
                            buffer.append("상세 :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "person") {
                            buffer.append("배우 :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "language") {
                            buffer.append("언어 :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "referenceIdentifier") {
                            buffer.append("레퍼런스 정보 :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        } else if (tag == "haspart") {
                            buffer.append("haspart :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "url") {
                            buffer.append("url :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "grade") {
                            buffer.append("grade :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "spatialCoverage") {
                            buffer.append("spatialCoverage :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "temporalCoverage") {
                            buffer.append("temporalCoverage :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "affiliation") {
                            buffer.append("affiliation :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }else if (tag == "subDescription") {
                            buffer.append("subDescription :")
                            xpp.next()
                            buffer.append(xpp.text) //mapy 요소의 TEXT 읽어와서 문자열버퍼에 추가
                            buffer.append("\n") //줄바꿈 문자 추가
                        }
                    }
                    XmlPullParser.TEXT -> {
                    }
                    XmlPullParser.END_TAG -> {
                        tag = xpp.name //테그 이름 얻어오기
                        if (tag == "item") buffer.append("\n") // 첫번째 검색결과종료..줄바꿈
                    }
                }
                eventType = xpp.next()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        buffer.append("파싱 끝\n")
        return buffer.toString() //StringBuffer 문자열 객체 반환
    } //getXmlData method....

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}